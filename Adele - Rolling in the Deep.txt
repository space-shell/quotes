Rolling in the Deep

Baby, I have no story to be told, but I've heard one on you. Now I'm gonna make your head burn. Think of me in the depths of your despair. Make a home down there as mine sure won't be shared.

      Adeleve with every piece of you. Don't underestimate the things that I will do.

      Adele