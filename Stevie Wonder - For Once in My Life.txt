For Once in My Life

I can touch what my heart used to dream of long before I knew someone warm like you.

      Stevie Wondernow won't desert me. I'm not alone anymore.

      Stevie Wonder