The Canterbury Tales

Purity in body and heart may please some - as for me, I make no boast. For, as you know, no master of a household has all of his utensils made of gold; some are wood, and yet they are of use.

      Geoffrey Chaucery that just as worms destroy a tree a wife destroys her husband and contrives, as husbands know, the ruin of their lives.

      Geoffrey Chaucerr