It's Still Rock and Roll to Me

Oh, what's the matter with the crowd I'm seeing? "Don't you know that they're out of touch?" Well, should I try to be a straight A student? "If you are then you think too much. Don't you know about the new fashion, honey? All you need are looks and a whole lotta money."

      Billy Joel