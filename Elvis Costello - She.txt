She

She may be the beauty or the beast. May be the famine or the feast. May turn each day into a heaven or a hell. She may be the mirror of my dream, a smile reflected in a stream. She may not be what she may seem inside her shell.

      Elvis Costelloay.

      Elvis Costellolo