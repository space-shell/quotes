The Man Who Fell to Earth

All I'm trying to say, Tommy, is that if you could just prove who you really are, you'd be free! Don't you understand, they don't understand you! They don't believe you. Believe me, they think you're one of us. They think you're a freak - or a fake. I know you're not. All you have to do is just prove it to 'em. Let 'em see you as you really are!

      Nicolas Roeg