Great Expectations

Estella, to the last hour of my life, you cannot choose but remain part of my character, part of the little good in me, part of the evil. But, in this separation I associate you only with the good, and I will faithfully hold you to that always, for you must have done me far more good than harm, let me feel now what sharp distress I may. O God bless you, God forgive you!

      Charles Dickens     Charles Dickens Dickenseen the embodiment of every graceful fancy that my mind has ever become acquainted with.

      Charles Dickens