Sorry Seems To Be the Hardest Word

What have I got to do to make you love me? What have I got to do to make you care? What do I do when lightning strikes me and I wake to find that you're not there?

      Elton John