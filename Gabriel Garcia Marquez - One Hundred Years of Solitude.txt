One Hundred Years of Solitude

In all the houses keys to memorizing objects and feelings had been written. But the system demanded so much vigilance and moral strength that many succumbed to the spell of an imaginary reality, one invented by themselves, which was less practical for them but more comforting.

      Gabriel Garcia Marquezuty on his skin. It was so deep in his body that the cracks in his skull did not give off blood but an amber-coloured oil that was impregnated with that secret perfume, and then they understood that the smell of Remedios the Beauty kept on torturing the men beyond death, right down to the dust of their bones.

      Gabriel Garcia Marquez