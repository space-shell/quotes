Notes to the Future: Words of Wisdom

Education is the most powerful weapon which you can use to change the world.

      Nelson Mandela