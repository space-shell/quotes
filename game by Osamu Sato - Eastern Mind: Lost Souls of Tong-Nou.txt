Eastern Mind: Lost Souls of Tong-Nou

You have saved me. Rin your soul, which has traveled the Tong-Nou World, is now more brilliant and more stronger than it has ever been. Now it is time to return to where you belong. Tong-Nou will return as the "Isle of Purgation." It shall never devour a soul again. Your soul returns, emitting beautiful light.

      game by Osamu Satoition. Eventually your soul will speak to you. The soul will speak of the time spent with each journey. Your life is one part of this enormous transmigration. The infinite repetition of life creates this enormous transmigration. All that has been accumulated is of value. Fortune and misfortune, pleasure and suffering, the useful and useless. All that has been acquired will be inherited to the next life. Your soul will continue this traveling eternally. Into the infinite and eternal future.

      game by Osamu Sato