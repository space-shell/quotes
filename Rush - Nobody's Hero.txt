Nobody's Hero

Hero - not the champion player who plays the perfect game. Not the glamour boy who loves to sell his name. Everybody's buying; nobody's hero.

      Rushry to hold some faith in the goodness of humanity.

      Rush