Gotham

Listen to the watch. The way it's ticking synchronizes with your heartbeat. Look into my eyes. Not above them, not around them, but deep into their center. You are completely relaxed and are becoming weightless. Are you ready to do something impossible?

      Bruno Heller