What Are People For?

Works of art participate in our lives; we are not just distant observers of their lives. They are in conversation among themselves and with us.

      Wendell Berry