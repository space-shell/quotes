Revolver

One thing I've learned in the last seven years: in every game and con there's always an opponent, and there's always a victim. The trick is to know when you're the latter, so you can become the former.

      Guy Ritchiepproval junkies. We're all in it for the slap on the back and the gold watch.

      Guy Ritchie