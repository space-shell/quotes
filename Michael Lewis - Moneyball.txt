Moneyball

If you challenge the conventional wisdom, you will find ways to do things much better than they are currently done.

      Michael Lewisto expect, you stand at least a chance of being inspired.

      Michael Lewis intellectual resources might have cured the common cold, or put a man on Pluto.

      Michael Lewisob simply by their appearance, you are less likely to find the best person for the job.

      Michael Lewis