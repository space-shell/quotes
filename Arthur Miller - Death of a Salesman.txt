Death of a Salesman

I stopped in the middle of that building and I saw - the sky. I saw the things that I love in this world. The work and the food and the time to sit and smoke. And I looked at the pen and I thought to myself, what the hell am I grabbing this for? Why am I trying to become what I don't want to be? What am I doing in an office, making a contemptuous, begging fool of myself, when all I want is out there, waiting for me the minute I say I know who I am!

      Arthur Miller