Seasons in the Sun

You gave me love and helped me find the sun, and every time that I was down you would always come around and get my feet back on the ground.

      Terry Jacksur hearts and skinned our knees. Goodbye my friend, it's hard to die when all the birds are singing in the sky. Now that spring is in the air, pretty girls are everywhere. Think of me and I'll be there.

      Terry Jacks