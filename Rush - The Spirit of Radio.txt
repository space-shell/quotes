The Spirit of Radio

Invisible airwaves crackle with life. Bright antennae bristle with the energy. Emotional feedback on a timeless wavelength bearing a gift beyond price almost free.

      Rushd. There is magic at your fingers for the spirit ever lingers undemanding contact in your happy solitude.

      Rushee.

      Rush