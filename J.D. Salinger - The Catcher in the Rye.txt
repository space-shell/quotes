The Catcher in the Rye

A lot of people, especially this one psychoanalyst guy they have here, keeps asking me if I'm going to apply myself when I go back to school next September. It's such a stupid question, in my opinion. I mean, how do you know what you're going to do till you do it? The answer is, you don't. I think I am, but how do I know? I swear it's such a stupid question.

      J.D. SalingerJ.D. Salingerey'd like it if you kidded them - in fact I know they would - but it's hard to get started, once you've known them a pretty long time and never kidded them.

      J.D. Salinger