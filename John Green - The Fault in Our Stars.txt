The Fault in Our Stars

You don't get to choose if you get hurt in this world, but you do have some say in who hurts you.

      John Greenurse, there is a bigger infinite set of numbers between 0 and 2, or between 0 and a million. Some infinities are bigger than other infinities.

      John Green on when I couldn't get my breath and it felt like my chest was on fire, flames licking the inside of my ribs fighting for a way to burn out of my body, my parents took me to the ER. Nurse asked me about the pain, and I couldn't even speak, so I held up nine fingers.

      John Green  John Green