True Love Will Find You in the End

This is a promise with a catch. Only if you're looking can it find you. 'Cause true love is searching too. But how can it recognize you unless you step out into the light? Don't be sad, I know you will be, but don't give until true love will find you in the end.

      Daniel Johnston