Saint Genet, Actor and Martyr

I maintain that inversion is the effect of neither a prenatal choice nor an endocrinal malformation nor even the passive and determined result of complexes. It is an outlet that a child discovers when he is suffocating.

      Jean Paul Sartre