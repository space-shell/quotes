Rope

I don't hold with the extremists who feel there should be open season for murder all year round. No, personally, I would prefer to have... "Cut a Throat Week" or "Strangulation Day."

      Alfred Hitchcockon, with the right to live and work and think as individuals, but with an obligation to the society we live in.

      Alfred HitchcockI don't know what you thought, but I know what you've done. You've murdered!

      Alfred Hitchcock