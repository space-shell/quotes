The Panda's Thumb: More Reflections in Natural History

I am, somehow, less interested in the weight and convolutions of Einstein's brain than in the near certainty that people of equal talent have lived and died in cotton fields and sweatshops.

      Stephen Jay Gould