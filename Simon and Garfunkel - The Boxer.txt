The Boxer

Though my story's seldom told, I have squandered my resistance. For a pocketful of mumbles such are promises, all lies and jest. Still, a man hears what he wants to hear and disregards the rest.

      Simon and Garfunkelfor the places only they would know.

      Simon and Garfunkelnkel