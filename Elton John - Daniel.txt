Daniel

You are older than me. Do you still feel the pain of the scars that won't heal? Your eyes have died but you see more than I. You're a star in the face of the sky.

      Elton John