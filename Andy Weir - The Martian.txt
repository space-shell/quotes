The Martian

Here's the cool part: I will eventually go to Schiaparelli and commandeer the Ares 4 lander. Nobody explicitly gave me permission to do this, and they can't until I'm aboard the Ares 4 and operating the comm system. After I board Ares 4, before talking to NASA, I will take control of a craft in international waters without permission. That makes me a pirate! A space pirate!

      Andy Weir