The Importance of Being Earnest

It is always painful to part from people whom one has known for a very brief space of time. The absence of old friends one can endure with equanimity.

      Oscar Wildery separation from anyone to whom one has just been introduced is almost unbearable.

      Oscar Wilde