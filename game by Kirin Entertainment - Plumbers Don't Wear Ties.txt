Plumbers Don't Wear Ties

Plumbers have everything: Greed, sex, spirituality, white-knuckled chases, shameful propositions, a nun, humor, true love, jaded love, taut action, comedy, a bad guy, a good guy, a hero, spine-tingling suspense, a hot babe, a damsel in distress, and a hollywood ending!

      game by Kirin Entertainment