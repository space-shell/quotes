We Are the Change We Seek: The Speeches of Barack Obama

There is not a liberal America and a conservative America -- there is the United States of America. There is not a Black America and a White America and Latino America and Asian America -- there's the United States of America.

      E. J. Dionne Jr. & Joy-Ann Reid