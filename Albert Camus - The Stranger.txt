The Stranger

What really counted was the possibility of escape, a leap of freedom, out of the implacable ritual, a wild run for it that would give whatever chance for hope there was. Of course, hope meant being cut down on some street corner, as you ran like mad, by a random bullet. But when I really thought it through, nothing was going to allow me such a luxury. Everything was against it; I would just be caught up in the machinery again.

      Albert Camusate they think they elect matter to me when we're all elected by the same fate, me and billions of privileged people like him who also called themselves my brothers?

      Albert Camus