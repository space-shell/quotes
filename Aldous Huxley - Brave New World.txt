Brave New World

As a victim, the Savage possessed, for Bernard, this enormous superiority over the others: that he was accessible. One of the principal functions of a friend is to suffer (in a milder and symbolic form) the punishments that we should like, but are unable, to inflict upon our enemies.

      Aldous Huxleyem... But you don't do either. Neither suffer nor oppose. You just abolish the slings and arrows. It's too easy.

      Aldous Huxley