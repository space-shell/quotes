My So-Called Life

Love is when you look into someone's eyes and suddenly you go all the way inside, to their soul, and you both know instantly. I always imagined I'd fall in love nursing a blind soldier who was wounded in battle. Or maybe while rescuing someone in the middle of a blizzard, seconds before the avalanche hits. I thought at least by the age of fifteen I'd have a love life, but I don't even have a like life!

      television series Winnie Holzman