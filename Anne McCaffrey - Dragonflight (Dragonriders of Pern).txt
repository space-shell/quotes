Dragonflight (Dragonriders of Pern)

When is a legend a legend? Why is a myth a myth? How old and disused must a fact be for it to be relegated to the category "Fairy-tale"?

      Anne McCaffrey