Perhaps Love

Perhaps love is like a resting place, a shelter from the storm. It exists to give you comfort. It is there to keep you warm.

      John Denver of trouble when you are most alone, the memory of love will bring you home.

      John Denver