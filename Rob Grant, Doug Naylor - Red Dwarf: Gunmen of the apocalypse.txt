Red Dwarf: Gunmen of the apocalypse

Bear Strangler McGee: [looming over Rimmer, who has just vomited into his hat] A man who beans up in the hat of Bear Strangler McGee is either mighty brave, or mighty stupid. Now which are you, boy? Rimmer: Sorry, what were the choices again? Lister: [quickly jumps in and hands McGee some money] You'll have to forgive our friend. He's a couple of gunmen short of a posse. Here. Bear Strangler McGee: That pays for the hat. Now what about the insult? Rimmer: OK, you're a fat, bearded git with breath that could paralyse a grizzly.

      Rob Grant, Doug Naylor