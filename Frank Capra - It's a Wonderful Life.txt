It's a Wonderful Life

You sit around here and you spin your little webs and you think the whole world revolves around you and your money. Well, it doesn't, Mr. Potter. In the whole vast configuration of things, I'd say you were nothing but a scurvy little spider!

      Frank Caprand live and die in a couple of decent rooms and a bath? Anyway, my father didn't think so. People were human beings to him. But to you, a warped, frustrated old man, they're cattle. Well in my book, my father died a much richer man than you'll ever be!

      Frank Capra