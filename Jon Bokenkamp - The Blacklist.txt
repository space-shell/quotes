The Blacklist

Walk on the wall again. Climb the tower. Ride the river. Stare at the frescoes. I want to sit in the garden and read one more good book.

      Jon Bokenkampto come, to stand at the helm of your destiny? I want that, one more time.

      Jon Bokenkamp