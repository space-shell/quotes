The Matrix

Are you sure you want to hear this? Morpheus believes in you, Neo. And no one, not you, not even me can convince him otherwise. He believes it so blindly that he's going to sacrifice his life to save yours.

      Larry and Andy Wachowski. Transcripted by Tim Staley