Charlotte's Web (1973 film)

You have been my friend. That in of itself is a tremendous thing. After all, what's life anyway? We're born, we live a little while, we die. The spider's life can't help being something of a miss. With all this trapping -- and eating flies. By helping you, perhaps I was trying to lift up my own life to trifle.

      Charles A. Nichols & Iwao Takamoto