The Complete Calvin and Hobbes

I hate all the rules and organization and teams and ranks in sports. Somebody's always yelling at you, telling you where to be, what to do, and when to do it. I figure when I want THAT, I'll join the army and at least get paid.

      Bill Watterson to make serious money and that's what's important about being an artist.

      Bill Watterson a reason for being. Doesn't it make you wonder?

      Bill Wattersonee thinker. He buys into the crass and shallow values art should transcend. He trades the integrity of his art for riches and fame. Oh, what the heck. I'll do it.

      Bill Watterson