The Design of Everyday Things

Peripheral vision and the feel of the keyboard provide some knowledge about key locations. Frequently used keys become completely learned, infrequently used keys are not learned well, and the other keys are partially learned. But as long as a typist needs to watch the keyboard, the speed is limited. The knowledge is still mostly in the world, not in the head.

      Don Normaninstruction, and typing speed increases notably, quickly surpassing handwriting speeds and, for some, reaching quite respectable rates.

      Don Norman